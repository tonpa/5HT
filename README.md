<h2 style="white-space: nowrap;"><a itemprop="sameAs" content="https://orcid.org/0000-0001-7127-8796" href="https://orcid.org/0000-0001-7127-8796" target="orcid.widget" rel="me noopener noreferrer" style="vertical-align:top;white-space: nowrap;">Namdak Tönpa <img src="https://orcid.org/sites/default/files/images/orcid_16x16.png"></a></h2>

The real fun was:

* Emeritus at Nyingma Longchen Nyingthig Ukraine <a href="https://longchenpa.guru">longchenpa.guru</a> for tibetan studies (2010—2016)
* Emeritus at Groupoid Infinity <a href="https://groupoid.space">groupoid.space</a> for formalization of mathematics (2016—2022)

While these were mostly hard time works:

* Author of N2O <a href="https://n2o.dev">n2o.dev</a> (2013—2022)
* Author of ERP.UNO <a href="https://erp.uno">erp.uno</a> (2015—2022)
* Author of Депозити ПриватБанк <a href="https://deposits.privatbank.ua/static/doc/index.htm">deposits.privatbank.ua</a> (2015—2016)
* Author of APL L1-interpreter and SMP/AMP real-time zero-copy runtime <a href="https://github.com/o83/n2o">platform.rs</a> (2016—2017)
* Author of FORMAL.UNO Монографія <a href="https://formal.uno">formal.uno</a> (2017—2019)
* Author of МІА: Документообіг <a href="https://crm.erp.uno">crm.erp.uno</a> (2019—2022)
* Author of Anders: Modal HoTT Proof Assistant <a href="https://homotopy.dev">homotopy.dev</a> (2021—2022)

